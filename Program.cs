﻿using System;

public sealed class Singleton
{
    // Private static instance to hold the single instance of the class.
    private static Singleton instance = null;

    // Private constructor to prevent creating instances outside of the class.
    private Singleton()
    {
        // Initialization code, if needed.
    }

    // Public method to get the instance of the Singleton class.
    public static Singleton Instance
    {
        get
        {
            // If the instance doesn't exist, create it.
            if (instance == null)
            {
                instance = new Singleton();
            }
            return instance;
        }
    }

    // Add your methods and properties here.
    public void DoSomething()
    {
        Console.WriteLine("Singleton instance is doing something.");
    }
}

public class Program
{
    public static void Main()
    {
        // Access the Singleton instance.
        Singleton singleton = Singleton.Instance;
        singleton.DoSomething();

        // Try to create another instance, but you'll get the same one.
        Singleton anotherSingleton = Singleton.Instance;
        anotherSingleton.DoSomething();
    }
}
